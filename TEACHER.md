---
module:			T-NSA-700
title:			devOps
subtitle:   README app

noFormalities: true

author:     ??
version:    0.1
---

# Teacher notes

## Run front

```
cd front/
yarn
npm start
```


## Run back

```
cd back/
composer install
php artisan serve
```

## Run db

```
docker run --rm -it -p 3306:3306 \
       -e MYSQL_ROOT_PASSWORD=toor \
       -e MYSQL_DATABASE=laravel \
       mysql \
       --default-authentication-plugin=mysql_native_password

# run migrations
cd back/
php artisan migrate
```
